import chalk from 'chalk';
import Koa from 'koa';

import isFunction from '@amphibian/is-function';
import {timeLogger} from '@amphibian/logger';
import parties from '@amphibian/party';

import * as env from './env';

import createHandler from './middleware/handler';
import createRouter from './middleware/route';

import logMiddleware from './middleware/log';
import errorMiddleware from './middleware/error';
import notFoundMiddleware from './middleware/not-found';

function createServer(options = {}) {
    const app = new Koa();
    const {registerMiddleware, registerHandler, handlerMiddleware} = createHandler(app);
    const router = createRouter(app);
    const startTime = Date.now();

    app.on('log', (...data) => {
        if ((options.logging !== 'errors') && (options.logging !== false)) {
            timeLogger.log(...data);

            if (env.isTest) {
                app.emit('logged', 'log');
            }
        }
    });

    app.on('error', (error) => {
        if ((error.log !== false) && (options.logging !== false)) {
            timeLogger.log(
                chalk.red('[Error]'),
                error.message,
                error.stack || ''
            );

            if (env.isTest) {
                app.emit('logged', 'error');
            }
        }
    });

    if ((options.logging !== false) && (options.logging !== 'errors')) {
        app.emit('log', chalk.green('Starting up...'));
        app.use(logMiddleware);
    }

    app.use(errorMiddleware);
    app.use(handlerMiddleware);
    app.use(notFoundMiddleware);

    let listener;

    if (options.listen !== false) {
        listener = app.listen(options.port || 3000, () => {
            app.emit('log',
                chalk.green('Now listening'),
                chalk.grey('at port'),
                chalk.yellow(listener.address().port),
                chalk.grey('after'),
                chalk.magenta(`${Date.now() - startTime}ms`)
            );
        });
    }

    return {
        env,
        app,
        listener,
        router,
        registerMiddleware,
        registerHandler,
        registerRouteHandler: (name, handler, options) => {
            if (!options && (isFunction(name))) {
                options = handler;
                handler = name;
                name = undefined;
            }

            return registerHandler(name, router(handler, options));
        },
        callback: () => {
            return app.callback();
        },
        close: () => {
            if (!listener) {
                return;
            }

            const party = parties(`server-close-${startTime}`);

            if (party.exists()) {
                return party.crash();
            }

            return party.host(new Promise((resolve) => {
                listener.close(resolve);
            }));
        }
    };
}

export default createServer;
