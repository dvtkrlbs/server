const environment = process.env.NODE_ENV;
const isDevelopment = environment === 'development';
const isProduction = environment === 'production';
const isTest = environment === 'test';

export {
    environment,
    isDevelopment,
    isProduction,
    isTest
};
