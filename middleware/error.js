/**
 * Error middleware that writes JSON errors to the client
 * @param {object} context
 * @param {function} next
**/
export default async function errorMiddleware(context, next) {
    try {
        await next();
    } catch (error) {
        context.status = error.status || 500;
        context.body = {
            error: {
                code: error.code || 'unknown_error',
                message: error.message
            }
        };

        if (context.status !== 404) {
            context.app.emit('error', error);
        }
    }
}
