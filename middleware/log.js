import chalk from 'chalk';
import objectKeys from '@amphibian/object-keys';

/**
 * Get status code colored to describe the status
 * @param {number} statusCode
 *
 * @returns {string} coloredStatusCode
**/
function getColoredStatusCode(statusCode = '') {
    const statusString = `(${statusCode.toString()})`;

    if (statusCode >= 500) {
        return chalk.red(statusString);
    } else if (statusCode >= 400) {
        return chalk.red(statusString);
    } else if (statusCode >= 300) {
        return chalk.yellow(statusString);
    } else if (statusCode >= 200) {
        return chalk.green(statusString);
    }

    return chalk.grey(statusString);
}

/**
 * Has host port?
 * @param {string} host
 *
 * @returns {boolean}
**/
function hasHostPort(host = '') {
    return Boolean(host.split(':')[1]);
}

/**
 * Get port from a host
 * @param {string} host
 *
 * @returns {string} port
**/
function getPortFromHost(host = '') {
    return host.split(':')[1];
}

/**
 * Return request body
**/
function getRequestBody(requestBody) {
    if (requestBody
    && (objectKeys(requestBody).length > 0)) {
        return [
            chalk.grey('BODY'),
            JSON.stringify(requestBody)
        ].join(' ');
    }

    return '';
}

/**
 * Log middleware that logs incoming requests
 * @param {object} context
 * @param {function} next
**/
async function logMiddleware(context, next) {
    const startTime = Date.now();

    await next();

    const elapsedRequestTime = (Date.now() - startTime) + 'ms';
    const requestIpAddress = (context.get('x-forwarded-for'))
        ? context.get('x-forwarded-for')
        : context.ip;

    const location = [];

    location.push(chalk.blue(context.protocol));
    location.push(chalk.black('://'));
    location.push(chalk.grey(context.hostname));

    if (hasHostPort(context.host)) {
        location.push(chalk.black(`:${getPortFromHost(context.host)}`));
    }

    location.push(chalk.yellow(context.url));

    context.app.emit('log',
        getColoredStatusCode(context.status),
        elapsedRequestTime,
        chalk.magenta(`[${requestIpAddress}]`),
        chalk.green(context.method),
        location.join(''),
        getRequestBody(context.request && context.request.body)
    );
}

export default logMiddleware;
export {
    getColoredStatusCode,
    hasHostPort,
    getPortFromHost,
    getRequestBody
};
