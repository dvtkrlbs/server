import test from 'ava';
import chalk from 'chalk';
import fetch from 'node-fetch';

import isObject from '@amphibian/is-object';
import isFunction from '@amphibian/is-function';

import createServer from '../server';

test.serial('createServer should return an object', (assert) => {
    const server = createServer();
    assert.true(isObject(server));
    return server.close();
});

test.serial('env should be an object', (assert) => {
    const server = createServer();
    assert.true(isObject(server.env));
    return server.close();
});

test.serial('app should be an object', (assert) => {
    const server = createServer();
    assert.true(isObject(server.app));
    return server.close();
});

test.serial('listener should be an object', (assert) => {
    const server = createServer();
    assert.true(isObject(server.listener));
    return server.close();
});

test.serial('listener should be undefined when options.listen is false', (assert) => {
    const server = createServer({listen: false});
    assert.true(server.listener === undefined);
    server.close();
});

test.serial('router should be a function', (assert) => {
    const server = createServer();
    assert.true(isFunction(server.router));
    return server.close();
});

test.serial('registerHandler should be a function', (assert) => {
    const server = createServer();
    assert.true(isFunction(server.registerHandler));
    return server.close();
});

test.serial('allow custom options.port', (assert) => {
    const server = createServer({port: 5000});
    assert.is(server.listener.address().port, 5000);
    return server.close();
});

test.serial('allow log disabling through options.logging', async (assert) => {
    const server = createServer({logging: false});

    server.app.on('log', () => {
        assert.fail('Did log');
    });

    await server.close().then(() => {
        assert.pass();
    });
});

test.serial('return koa callback when calling callback', (assert) => {
    const server = createServer({listen: false});
    assert.true(server.callback() !== undefined);
});

test.serial.cb('should log when starting up', (assert) => {
    assert.plan(1);

    const server = createServer();

    server.app.on('log', async (...data) => {
        await server.close();
        assert.end(assert.is(data[0], chalk.green('Now listening')));
    });
});

test.serial.cb('should log when an error occurs', (assert) => {
    assert.plan(1);
    const server = createServer();

    server.app.on('error', async (error) => {
        await server.close();
        assert.end(assert.is(error.message, 'hello'));
    });

    server.app.emit('error', new Error('hello'));
});

test.serial.cb('should not log when an error occurs and logging is false', (assert) => {
    const server = createServer({logging: false});

    server.app.on('logged', () => {
        assert.fail();
    });

    server.app.emit('error', new Error('hello'));

    setTimeout(async () => {
        await server.close();
        assert.end();
    }, 250);
});

test.serial.cb('should only log errors logging is set to errors', (assert) => {
    const server = createServer({logging: 'errors'});

    server.app.on('logged', (type) => {
        if (type !== 'error') {
            assert.fail();
        }
    });

    server.app.emit('error', new Error('hello'));
    server.app.emit('log', 'hello');

    setTimeout(async () => {
        await server.close();
        assert.end();
    }, 250);
});

test.serial.cb('should not log error if `error.log` is false', (assert) => {
    assert.plan(1);
    const server = createServer();

    server.app.on('logged', (type) => {
        if (type === 'error') {
            assert.fail();
        }
    });

    const silentError = new Error('hello');
    silentError.log = false;
    server.app.emit('error', silentError);

    setTimeout(async () => {
        await server.close();
        assert.end(assert.pass());
    }, 500);
});

test.serial('registerRouteHandler should be a function', (assert) => {
    const server = createServer();
    assert.true(isFunction(server.registerRouteHandler));
    return server.close();
});

test.serial('registerRouterHandler should bind named handler route', async (assert) => {
    const server = createServer();
    let routed = false;

    server.registerRouteHandler('my_handler', () => {
        routed = true;
    }, {method: 'GET', path: '/test'});

    await fetch(`http://localhost:${server.listener.address().port}/test`);

    assert.true(routed);
    return server.close();
});

test.serial('registerRouterHandler should bind unnamed handler route', async (assert) => {
    const server = createServer();
    let routed = false;

    server.registerRouteHandler(() => {
        routed = true;
    }, {method: 'GET', path: '/test'});

    await fetch(`http://localhost:${server.listener.address().port}/test`);

    assert.true(routed);
    return server.close();
});

test.serial('close should finish ongoing requests and prevent further requests', async (assert) => {
    const server = createServer();
    const {port} = server.listener.address();
    let routed = 0;

    server.registerRouteHandler(() => {
        server.close();
        routed += 1;
    }, {method: 'GET', path: '/test'});

    await fetch(`http://localhost:${port}/test`);

    try {
        await fetch(`http://localhost:${port}/test`);
    } catch (error) {
        assert.is(routed, 1);
        return;
    }

    assert.fail('No error thrown');
});

test.serial('multiple server close requests should be handled as one', async (assert) => {
    const server = createServer();
    const {port} = server.listener.address();
    let routed = 0;

    server.registerRouteHandler(() => {
        server.close();
        routed += 1;
    }, {method: 'GET', path: '/test'});

    await Promise.all([
        fetch(`http://localhost:${port}/test`),
        fetch(`http://localhost:${port}/test`)
    ]);

    assert.is(routed, 2);
});
