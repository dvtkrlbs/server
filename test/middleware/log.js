import EMPTY_STRING from 'empty-string';
import chalk from 'chalk';
import test from 'ava';

import logMiddleware, {
    getColoredStatusCode,
    hasHostPort,
    getPortFromHost,
    getRequestBody
} from '../../middleware/log';

test.cb.serial('should log', (assert) => {
    const context = {
        method: 'GET',
        status: 200,
        protocol: 'https',
        hostname: 'localhost.com',
        host: 'localhost.com',
        url: '/folder?query',
        ip: '127.0.0.1',

        get: () => '',
        app: {
            emit: (event, ...data) => {
                assert.end(assert.is(data.join(EMPTY_STRING), [
                    getColoredStatusCode(context.status),
                    data[1], // elapsed time is not worth testing
                    chalk.magenta(`[${context.ip}]`),
                    chalk.green(context.method),
                    chalk.blue(context.protocol),
                    chalk.black('://'),
                    chalk.grey(context.hostname),
                    chalk.yellow(context.url)
                ].join(EMPTY_STRING)));
            }
        }
    };

    try {
        logMiddleware(context, async () => {});
    } catch (error) {
        assert.fail(error);
    }
});

test.cb.serial('should log with port name', (assert) => {
    const context = {
        method: 'GET',
        status: 200,
        protocol: 'https',
        hostname: 'localhost.com',
        host: 'localhost.com:3000',
        url: '/folder?query',
        ip: '127.0.0.1',

        get: () => '',
        app: {
            emit: (event, ...data) => {
                assert.end(assert.is(data.join(EMPTY_STRING), [
                    getColoredStatusCode(context.status),
                    data[1], // elapsed time is not worth testing
                    chalk.magenta(`[${context.ip}]`),
                    chalk.green(context.method),
                    chalk.blue(context.protocol),
                    chalk.black('://'),
                    chalk.grey(context.hostname),
                    chalk.black(`:${getPortFromHost(context.host)}`),
                    chalk.yellow(context.url)
                ].join(EMPTY_STRING)));
            }
        }
    };

    try {
        logMiddleware(context, async () => {});
    } catch (error) {
        assert.fail(error);
    }
});

test.cb.serial('should log after', (assert) => {
    let didHappen = false;
    const context = {
        get: () => '',
        app: {
            emit: () => {
                if (didHappen) {
                    assert.end(assert.pass());
                } else {
                    assert.end(assert.fail('Was called first'));
                }
            }
        }
    };

    try {
        logMiddleware(context, async () => {
            didHappen = true;
        });
    } catch (error) {
        console.log(error);
        // ... do nothing
    }
});

test('should allow things to happen after it', async (assert) => {
    let didHappen = false;

    try {
        await logMiddleware({}, async () => {
            didHappen = true;
        });
    } catch (error) {
        // ... do nothing
    }

    if (didHappen) {
        assert.pass();
    } else {
        assert.fail();
    }
});

test('get colored status code for 100 Continue', (assert) => {
    assert.is(getColoredStatusCode(100), chalk.grey('(100)'));
});

test('get colored status code for 200 OK', (assert) => {
    assert.is(getColoredStatusCode(200), chalk.green('(200)'));
});

test('get colored status code for 300 Multiple Choices', (assert) => {
    assert.is(getColoredStatusCode(300), chalk.yellow('(300)'));
});

test('get colored status code for 400 Bad Request', (assert) => {
    assert.is(getColoredStatusCode(400), chalk.red('(400)'));
});

test('get colored status code for 500 Internal Server Error', (assert) => {
    assert.is(getColoredStatusCode(500), chalk.red('(500)'));
});

test('check for port in hostName', (assert) => {
    assert.true(hasHostPort('localhost:3000'));
});

test('check for port in hostName where there is no port', (assert) => {
    assert.false(hasHostPort('localhost'));
});

test('get port from hostName', (assert) => {
    assert.is(getPortFromHost('localhost:3000'), '3000');
});

test('get request body', (assert) => {
    const _requestBody = {hello: true};

    assert.is(getRequestBody(_requestBody), [
        chalk.grey('BODY'),
        JSON.stringify(_requestBody)
    ].join(' '));
});

test('get empty string when there is no request body', (assert) => {
    assert.is(getRequestBody(), EMPTY_STRING);
});
