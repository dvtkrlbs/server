import Koa from 'koa';
import test from 'ava';

import createRouter, {routeMiddleware} from '../../middleware/route';

test('should copy handler name', (assert) => {
    const route = createRouter(new Koa());
    const myRoute = (context) => {
        context.body = 'test';
    };

    assert.is(route(myRoute, {path: '/test'}).name, 'myRoute');
});

test('should bind to paths', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test'};

    await route(async (context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test'})(context, () => {
        assert.fail('Next was called');
    });

    assert.true(context.routed);
});

test('should default method to GET', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test'};

    await route(async (context) => {
        context.routed = true;
    }, {path: '/test'})(context, () => {
        assert.fail('Next was called');
    });

    assert.true(context.routed);
});

test('should set context.routePath', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test'};

    await route(async (context, next) => {
        context.routed = true;
        await next();
    }, {method: 'GET', path: '/test'})(context, () => {
        context.nexted = true;
        assert.is(context.routePath, '/test');
    });

    if (!context.routed) {
        assert.fail('Not routed.');
    }

    if (!context.nexted) {
        assert.fail('Not nexted.');
    }
});

test('context.args should default to null with string path', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test'};

    await route(() => {}, {
        method: 'GET',
        path: '/test'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(context.args, null);
});

test('should bind to string paths with parameters', async (assert) => {
    const route = createRouter(new Koa());
    const body = 'hello';
    const context = {method: 'GET', path: '/test/hello'};

    await route((context) => {
        context.body = body;
    }, {
        method: 'GET',
        path: '/test/:someParameter'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(context.body, body);
});

test('should bind to garbled string paths with parameters', async (assert) => {
    const route = createRouter(new Koa());
    const body = 'hello';
    const context = {method: 'GET', path: '/test/\\\\$$$$hel?l?o?/:hello'};

    await route((context) => {
        context.body = body;
    }, {
        method: 'GET',
        path: '/test/\\\\$$$$hel?l?o?/:hello'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(context.body, body);
});

test('should put string path parameters in args object', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/hello'};

    await route(() => {}, {
        method: 'GET',
        path: '/test/:someParameter'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.deepEqual(context.args, {someParameter: 'hello'});
});

test('should put multiple string path parameters in args object', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/hello/then/world'};

    await route(() => {}, {
        method: 'GET',
        path: '/test/:someParameter/then/:someOtherParameter'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.deepEqual(context.args, {
        someParameter: 'hello',
        someOtherParameter: 'world'
    });
});

test('should set RegExp context.routePath', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/something'};

    const routeRegExp = /^\/test\/([^/]+)$/;

    await route(async (context, next) => {
        context.routed = true;
        await next();
    }, {method: 'GET', path: routeRegExp})(context, () => {
        context.nexted = true;
        assert.is(context.routePath, routeRegExp);
    });

    if (!context.routed) {
        assert.fail('Not routed.');
    }

    if (!context.nexted) {
        assert.fail('Not nexted.');
    }
});

test('should bind regexp path', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/123'};

    await route(async (context) => {
        context.routed = true;
    }, {method: 'GET', path: /^\/test\/([\d]+)$/})(context, () => {
        assert.fail('Next was called');
    });

    assert.true(context.routed);
});

test('should pass regexp match to context', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/123'};

    await route(async () => {}, {
        method: 'GET',
        path: /^\/test\/([\d]+)$/
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(JSON.stringify(context.args), JSON.stringify(['123']));
});

test('should pass multiple regexp matches to context', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/123/456/789'};

    await route(async () => {}, {
        method: 'GET',
        path: /^\/test\/([\d]+)\/([\d]+)\/([\d]+)$/
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(JSON.stringify(context.args), JSON.stringify(['123', '456', '789']));
});

test('should leak when path does not match', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test-2'};

    await route(async (context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test'})(context, (context) => {
        assert.is(context.routed, undefined);
    });

    if (context.routed) {
        assert.fail();
    }
});

test('should leak when string path parameters do not match', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/hello'};

    await route((context) => {
        context.routed = true;
    }, {
        method: 'GET',
        path: '/test/:someParameter/then/:someOtherParameter'
    })(context, () => {
        assert.is(context.routed, undefined);
    });

    if (context.routed) {
        assert.fail();
    }
});

test('should leak when string path parameters is deeper', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/hello/then/world'};

    await route((context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test/:someParameter'})(context, () => {
        assert.is(context.routed, undefined);
    });

    if (context.routed) {
        assert.fail();
    }
});

test('should leak when string path parameter is undefined', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test/'};

    await route((context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test/:someParameter'})(context, () => {
        assert.is(context.routed, undefined);
    });

    if (context.routed) {
        assert.fail();
    }
});

test('should leak when string path parameter is empty', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'GET', path: '/test//'};

    await route((context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test/:someParameter'})(context, () => {
        assert.is(context.routed, undefined);
    });

    if (context.routed) {
        assert.fail();
    }
});

test('should throw when method does not match', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'POST', path: '/test'};

    try {
        await route(async (context) => {
            context.routed = true;
        }, {method: 'GET', path: '/test'})(context, () => {});
    } catch (error) {
        assert.is(error.message, 'Method Not Allowed (method_not_allowed)');
        assert.is(error.code, 'method_not_allowed');
        assert.is(error.status, 405);

        return;
    }

    assert.fail('No error thrown');
});

test('should not throw when method does not match one route method but matches another', (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'POST', path: '/test'};

    const didThrow = false;
    const firstRoute = route(async () => {}, {
        method: 'GET',
        path: '/test'
    });

    route(async () => {}, {
        method: 'POST',
        path: '/test'
    });

    firstRoute(context, async () => {});
    assert.false(didThrow);
});

test('should throw when method does not match for multiple routes', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'DELETE', path: '/test'};

    try {
        const firstRoute = route(async () => {}, {
            method: 'GET',
            path: '/test'
        });

        route(async () => {}, {
            method: 'POST',
            path: '/test'
        });

        await firstRoute(context, () => {});
    } catch (error) {
        assert.is(error.message, 'Method Not Allowed (method_not_allowed)');
        assert.is(error.code, 'method_not_allowed');
        assert.is(error.status, 405);

        return;
    }

    assert.fail('No error thrown');
});

test('should automatically resolve OPTIONS request', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'OPTIONS', path: '/test'};

    await route(async (context) => {
        context.routed = true;
    }, {method: 'GET', path: '/test'})(context, () => {
        assert.fail('Next was called');
    });

    assert.is(context.body, 'GET');
    assert.is(context.status, 200);
});

test('should resolve OPTIONS request when multiple methods are allowed', async (assert) => {
    const route = createRouter(new Koa());
    const context = {method: 'OPTIONS', path: '/test'};

    await route(async () => {}, {
        method: 'GET',
        path: '/test'
    })(context, () => {
        assert.fail('Next was called');
    });

    await route(async () => {}, {
        method: 'POST',
        path: '/test'
    })(context, () => {
        assert.fail('Next was called');
    });

    assert.is(context.body, 'GET, POST');
    assert.is(context.status, 200);
});

test('failing: should require an app object', (assert) => {
    try {
        createRouter();
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'app');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should not allow invalid type app', (assert) => {
    try {
        createRouter([]);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'app');
        assert.is(error.data[1], 'object');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require an app object on the middleware', (assert) => {
    try {
        routeMiddleware();
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'app');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should not allow invalid type app on the middleware', (assert) => {
    try {
        routeMiddleware([]);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'app');
        assert.is(error.data[1], 'object');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require a paths object on the middleware', (assert) => {
    try {
        routeMiddleware({});
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'paths');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should not allow invalid type paths on the middleware', (assert) => {
    try {
        routeMiddleware({}, []);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'paths');
        assert.is(error.data[1], 'object');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require a handler', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route();
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'handler');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require a handler of type function', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route([]);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'handler');
        assert.is(error.data[1], 'function');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require options', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route(() => {});
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'options');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require an options object', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route(() => {}, []);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'options');
        assert.is(error.data[1], 'object');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should not allow methods that are not strings', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route(async () => {}, {
            method: () => {}
        });
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'options.method');
        assert.is(error.data[1], 'string');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should require a path', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route(async () => {}, {});
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.is(error.data[0], 'options.path');

        return;
    }

    assert.fail('No error thrown');
});

test('failing: should not allow paths that are neither string nor regexp', async (assert) => {
    const route = createRouter(new Koa());

    try {
        await route(async () => {}, {
            method: 'POST',
            path: () => {}
        });
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.is(error.data[0], 'options.path');
        assert.is(error.data[1], 'string|regexp');

        return;
    }

    assert.fail('No error thrown');
});
