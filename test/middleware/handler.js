import Koa from 'koa';
import test from 'ava';

import createHandler, {registerHandler as registerHandlerFromScratch} from '../../middleware/handler';

test('failing: createHandler with no app', (assert) => {
    try {
        createHandler();
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.deepEqual(error.data, ['app']);
    }
});

test('failing: createHandler with invalid type app', (assert) => {
    try {
        createHandler([]);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['app', 'object']);
    }
});

test('should work with no handler', async (assert) => {
    const {handlerMiddleware} = createHandler(new Koa());
    assert.plan(1);

    await handlerMiddleware({}, async () => {
        assert.pass();
    });
});

test('should add handler', (assert) => {
    const {handlers, registerHandler} = createHandler(new Koa());
    const handler = async () => {};

    registerHandler(handler);
    assert.deepEqual(handlers, [handler]);
});

test('should add named handler', (assert) => {
    const {handlers, registerHandler} = createHandler(new Koa());
    const handler = async () => {};

    registerHandler('named-handler', handler);
    assert.deepEqual(handlers, [handler]);
});

test('should call handlers', async (assert) => {
    const {registerHandler, handlerMiddleware} = createHandler(new Koa());
    const context = {};

    registerHandler(async (context, next) => {
        context.handler = true;
        await next();
    });

    await handlerMiddleware(context, async (context) => {
        assert.true(context.handler);
    });
});

test('should call handlers consecutively', async (assert) => {
    const {registerHandler, handlerMiddleware} = createHandler(new Koa());
    const context = {order: [0]};

    registerHandler(async (context, next) => {
        context.order.push(1);
        await next();
    });

    registerHandler(async (context, next) => {
        context.order.push(2);
        await next();
    });

    await handlerMiddleware(context, async (context) => {
        context.order.push(3);
        assert.deepEqual(context.order, [0, 1, 2, 3]);
    });
});

test('should log handler name when a new handler was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerHandler} = createHandler(app);
    const handler = async () => {};

    app.on('handlers(handler):did-add', (name) => {
        assert.is('handler', name);
    });

    registerHandler(handler);
});

test('should log anonymous handler name when a new handler was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerHandler} = createHandler(app);

    app.on('handlers(handler):did-add', (name) => {
        assert.is('anonymous', name);
    });

    registerHandler(() => true);
});

test('should log provided handler name when a new handler was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerHandler} = createHandler(app);
    const handlerName = 'named-handler';
    const handler = async () => {};

    app.on('handlers(handler):did-add', (name) => {
        assert.is(handlerName, name);
    });

    registerHandler(handlerName, handler);
});

test('should log middleware name when a new middleware was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerMiddleware} = createHandler(app);
    const middleware = async () => {};

    app.on('handlers(middleware):did-add', (name) => {
        assert.is('middleware', name);
    });

    registerMiddleware(middleware);
});

test('should log anonymous middleware name when a new middleware was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerMiddleware} = createHandler(app);

    app.on('handlers(middleware):did-add', (name) => {
        assert.is('anonymous', name);
    });

    registerMiddleware(() => true);
});

test('should log provided middleware name when a new middleware was added', (assert) => {
    assert.plan(1);

    const app = new Koa();
    const {registerMiddleware} = createHandler(app);
    const middlewareName = 'named-middleware';
    const middleware = async () => {};

    app.on('handlers(middleware):did-add', (name) => {
        assert.is(middlewareName, name);
    });

    registerMiddleware(middlewareName, middleware);
});

test('should await async handlers', async (assert) => {
    const {registerHandler, handlerMiddleware} = createHandler(new Koa());
    const context = {order: [0]};

    registerHandler(async (context, next) => {
        await new Promise((resolve) => {
            setTimeout(() => {
                context.order.push(1);
                resolve();
            });
        });

        context.order.push(2);
        await next();
    });

    registerHandler(async (context, next) => {
        context.order.push(3);
        await next();
    });

    await handlerMiddleware(context, async (context) => {
        context.order.push(4);
        assert.deepEqual(context.order, [0, 1, 2, 3, 4]);
    });
});

test('should stop if next is not called', async (assert) => {
    const {registerHandler, handlerMiddleware} = createHandler(new Koa());
    assert.plan(1);

    registerHandler(async () => {
        await new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, 500);
        });

        assert.pass();
    });

    registerHandler(async () => {
        assert.fail('Next was called');
    });

    await handlerMiddleware({}, async () => {
        assert.fail('Next was called');
    });
});

test('failing: registerHandler with no app provided', (assert) => {
    try {
        registerHandlerFromScratch();
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.deepEqual(error.data, ['app']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with invalid type app provided', (assert) => {
    try {
        registerHandlerFromScratch([]);
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['app', 'object']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with no handlers provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa());
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.deepEqual(error.data, ['handlers']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with invalid type handlers provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), {});
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['handlers', 'array']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with no type provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), []);
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.deepEqual(error.data, ['type']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with invalid type type provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), [], {});
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['type', 'string']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with invalid type name provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), [], 'test', {});
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['name', 'string']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with no handler provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), [], 'test', '');
    } catch (error) {
        assert.is(error.code, 'missing_required_parameters');
        assert.deepEqual(error.data, ['handler']);

        return;
    }

    assert.fail('No error thrown');
});

test('failing: registerHandler with invalid type handler provided', (assert) => {
    try {
        registerHandlerFromScratch(new Koa(), [], 'test', '', {});
    } catch (error) {
        assert.is(error.code, 'type_error');
        assert.deepEqual(error.data, ['handler', 'function']);

        return;
    }

    assert.fail('No error thrown');
});
